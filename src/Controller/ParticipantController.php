<?php

namespace App\Controller;

use App\Entity\Campus;
use App\Entity\Participant;
use App\Entity\Sortie;
use App\Form\ParticipantType;
use App\Form\SortieType;
use App\Repository\ParticipantRepository;
use App\Security\AppAuthenticator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;

/**
 * @Route("/monprofil", name="profil_")
 */
class ParticipantController extends AbstractController
{
    private UserPasswordEncoderInterface $encoder;
    public function __construct(UserPasswordEncoderInterface $encoder, EntityManagerInterface $manager, ParticipantRepository $repository)
    {
        $this->encoder = $encoder;
        $this->repository = $repository;
        $this->manager = $manager;
    }



    /*
     *
     *
     * /**
     * @Route("/details/{id}", name="details", requirements={"id"="\d+"})

    public function details(int $id, ParticipantRepository $participantRepository, EntityManagerInterface $entityManager): Response
    {
        $participant = $entityManager->getRepository(Participant::class)->find($id);
        $profil = $participantRepository->findAll();
        if (!$profil) {
            throw $this->createNotFoundException('Oh no, you dont find this participant !!');
        }
        //dump($profil);

        return $this->render('participant/monProfil.html.twig', [
            "participant" => $profil
        ]);

    }
     *
     *
     *
     */



    /**
     * @Route("/creation", name="demo")
     */
    public function demo(EntityManagerInterface $entityManager): Response
    {
        //créer une instance de mon entité
        $participant = new Participant();

        $campus = new Campus();
        $campus->setNom("France");

        //hydrate toutes les propriétés
        $participant->setNom('Salvatore');
        $participant->setRoles(["ROLE_USER"]);
        $participant->setPrenom('Sara');
        $participant->setCampus($campus);
        $participant->setEmail('salsara@eni.fr');
        $plainPassword = 'testroot';
        $encoded = $this->encoder->encodePassword($participant, $plainPassword);
        $participant->setPassword($encoded);
        $participant->setTelephone('01 01 01 01 01');
        $participant->setAdministrateur(false);
        $participant->setActif(true);

        dump($participant);

        $entityManager->persist($participant);
        $entityManager->flush();

        dump($participant);

        //$entityManager->remove($serie);
        //$serie->setGenres('comedy');
        // $entityManager->flush();

        //$entityManager = $this->getDoctrine()->getManager();


        return $this->render('main/home.html.twig');
    }


    /**
     * @Route("/modification/{id}", name="modification", requirements={"id"="\d+"})
     */
    public function modifier(int $id,
                             Request $request,
                             EntityManagerInterface $entityManager
    ): Response
    {
        $participant = $entityManager->getRepository(Participant::class)->find($id);

        if (!$participant) {
            throw $this->createNotFoundException("L'utilisateur n'a pas été trouvé");
        }

        $participantForm = $this->createForm(ParticipantType::class, $participant);
        $participantForm->handleRequest($request);

        if ($participantForm->isSubmitted() && $participantForm->isValid()) {
            if ($request->request->get('btnPublier')) {

                $entityManager->persist($participant);

            } elseif ($request->request->get('btnSupprimer')) {

                $entityManager->remove($participant);
            }


            $entityManager->flush();

            $this->addFlash('success', 'Participant modifié');

            return $this->redirectToRoute("main_home");
        }
        return $this->render("participant/modifier.html.twig", [
            'participantForm' => $participantForm->createView()
        ]);


    }


}
