<?php

namespace App\Controller;

use App\Entity\Participant;
use App\Form\ParticipantType;
use App\Repository\ParticipantRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin", name="admin_")
 */
class AdminController extends AbstractController
{


    /**
     * @Route("", name="admin")
     */
    public function index(): Response
    {
        return $this->render('admin/listUsers.html.twig', [
            'controller_name' => 'AdminController',
        ]);
    }


    /**
     * @Route("/listUsers", name="list")
     */
    public function list(ParticipantRepository $participantRepository): Response
    {

        //todo : aller chercher les series en bdd
        $participant = $participantRepository->findAll();
        //dd($series);
        return $this->render('admin/listUsers.html.twig', [
            "participants" => $participant
        ]);
    }


    /**
     * @Route("/detailsUser/{id}", name = "details")
     */
    public function details(int $id, ParticipantRepository $participantRepository): Response
    {
        //todo : aller chercher la serie dans la bdd
        $participant = $participantRepository->find($id);
        if (!$participant) {
            throw $this->createNotFoundException('Oh no !!');
        }


        return $this->render('admin/details.html.twig', [
            "participant" => $participant
        ]);
    }



    /**
     * @Route("/supprimer/{id}", name="supprimer", requirements={"id"="\d+"})
     */
    public function supprimer(int $id,
                             Request $request,
                             EntityManagerInterface $entityManager
    ): Response
    {
        $participant = $entityManager->getRepository(Participant::class)->find($id);

        if (!$participant) {
            throw $this->createNotFoundException("L'utilisateur n'a pas été trouvé");
        }

        $participantForm = $this->createForm(ParticipantType::class, $participant);
        $participantForm->handleRequest($request);

        if ($participantForm->isSubmitted() && $participantForm->isValid()) {
            if ($request->request->get('btnSupprimer')) {

                $entityManager->remove($participant);

            }


            $entityManager->flush();

            $this->addFlash('success', 'Participant supprimé');

            return $this->redirectToRoute("main_home");
        }
        return $this->render("admin/listUsers.html.twig", [
            'participantForm' => $participantForm->createView()
        ]);


    }


}
