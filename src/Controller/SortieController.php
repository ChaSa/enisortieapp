<?php


namespace App\Controller;


use App\Entity\Etat;
use App\Entity\Ville;
use App\Entity\Sortie;
use App\Form\EtatType;
use App\Form\SortieType;
use App\Form\VilleType;
use App\Repository\EtatRepository;
use App\Repository\ParticipantRepository;
use App\Repository\SortieRepository;
use Doctrine\ORM\EntityManagerInterface;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/sortie", name="sortie_")
 */
class SortieController extends AbstractController
{

    /**
     * @Route ("/creer", name="creer")
     */
    public function creer(
        Request $request,
        EntityManagerInterface $entityManager
    ): Response
    {
        $sortie = new Sortie();

//find etat() todo au lieu d'un create
        $etat = new Etat();
        $etat->setLibelle('Creee');
        $sortie->setEtat($etat);

        $idParticipant = $entityManager->getReference('App\Entity\Participant', $this->getUser()->getId());
        $sortie->setParticipant($idParticipant);

        $sortieForm = $this->createForm(SortieType::class, $sortie);


        $sortieForm->handleRequest($request);

        if ($sortieForm->isSubmitted() && $sortieForm->isValid()) {

           // $clicked = $request->request->get('clicked');
            //dd($clicked);
            if ($request->request->get('clicked') == 'publier') {
                $etat->setLibelle('Ouverte');
                $sortie->setEtat($etat);
            }

            $entityManager->persist($sortie);
            $entityManager->flush();

            $this->addFlash('success', 'Sortie créée');
            return $this->redirectToRoute('sortie_details', ['id' => $sortie->getId()]);
        }

        return $this->render('sortie/creer.html.twig', [
            'sortieForm' => $sortieForm->createView(),

        ]);
    }


    /**
     * @Route ("/details/{id}", name="details", requirements={"id"="\d+"})
     */

    public function details(
        int $id,
        SortieRepository $sortieRepository
    ): Response
    {
        $sortie = $sortieRepository->find($id);
        return $this->render('sortie/details.html.twig', [
            "sortie" => $sortie,
        ]);
    }

    /**
     * @Route ("/modifier/{id}", name="modifier", requirements={"id"="\d+"})
     */

    //Analyse du contexte à ajouter
    public function modifier(
        int $id,
        Request $request,
        EntityManagerInterface $entityManager
    ): Response
    {
        $sortie = $entityManager->getRepository(Sortie::class)->find($id);

        if (!$sortie) {
            throw $this->createNotFoundException("La sortie n'a pas été trouvée");
        }

        $sortieForm = $this->createForm(SortieType::class, $sortie);
        $sortieForm->handleRequest($request);

        if ($sortieForm->isSubmitted() && $sortieForm->isValid()) {
            if ($request->request->get('btnPublier')) {
                $etat = $sortie->getEtat();
                $etat->setLibelle('Ouverte');
                $sortie->setEtat($etat);
                $entityManager->persist($sortie);

            } elseif ($request->request->get('btnSupprimer')) {
                $etat = $sortie->getEtat();
                if ($etat->getLibelle() == "Ouverte") {
                    $etat->setLibelle('Annulée');
                    $sortie->setEtat($etat);
                    $entityManager->persist($sortie);
                }
                $entityManager->remove($sortie);
            }


            $entityManager->flush();

            $this->addFlash('success', 'Sortie modifiée');

            return $this->redirectToRoute("main_home");
        }
        return $this->render("sortie/modifier.html.twig", [
            'sortieForm' => $sortieForm->createView()
        ]);
    }

    /**
     * @Route ("/annuler/{id}", name="annuler", requirements={"id"="\d+"})
     */
    public function annuler(
        int $id,
        Request $request,
        SortieRepository $sortieRepository,
        EtatRepository $etatRepository,
        EntityManagerInterface $entityManager
    ): Response
    {
        $sortie = $sortieRepository->find($id);

        $idEtat = $sortie->getEtat()->getId();
        $etat = $etatRepository->find($idEtat);

        $etatForm = $this->createForm(EtatType::class, $etat);
        $etatForm->handleRequest($request);
        if ($etatForm->isSubmitted() && $etatForm->isValid()) {

            if (($sortie->getEtat()->getLibelle() == "Ouverte")){
            $etat->setLibelle('Annulée');
            $entityManager->persist($etat);
            $entityManager->flush();

            $this->addFlash('success', 'Sortie annulée');
            }
            $entityManager->remove($sortie);
            $entityManager->flush();

            return $this->redirectToRoute('main_home');
        }
        return $this->render('sortie/annuler.html.twig', [
            'sortie' => $sortie,
            'etatForm' => $etatForm->createView()
        ]);
    }

    /**
     * @Route ("/inscription/{id}", name="inscription")
     */

    public function inscriptions(
        int $id,
        SortieRepository $sortieRepository,
        ParticipantRepository $participantRepository,
        EntityManagerInterface $entityManager
    ): Response
    {
        $sortie = $sortieRepository->find($id);

        if ($sortie->getEtat()->getLibelle() == 'Ouverte' && count($sortie->getParticipants()) < $sortie->getNbInscriptionsMax()) {

            $user = $this->getUser()->getId();
            $participant = $participantRepository->find($user);
            $sortie->addParticipant($participant);
            $entityManager->persist($sortie);
            $entityManager->flush();
            $this->addFlash('success', 'Incription realisee');
        } else {
            $this->addFlash('danger', 'Pas possible de sinscrire');
        }


        return $this->redirectToRoute('main_home');
    }


    /**
     * @Route("/desincrire/{id}", name="desincrire")
     */
    public function desincrire(
        int $id,
        SortieRepository $sortieRepository,
        ParticipantRepository $participantRepository,
        EntityManagerInterface $entityManager
    ): Response
    {
        $sortie = $sortieRepository->find($id);

            $user = $this->getUser()->getId();
            $participant = $participantRepository->find($user);
            $sortie->removeParticipant($participant);
            $entityManager->persist($sortie);
            $entityManager->flush();

        return $this->redirectToRoute('main_home');
    }


}