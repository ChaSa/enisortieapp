<?php


namespace App\Controller\Api;


use App\Repository\LieuRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api")
 */
class ApiLieuController extends \Symfony\Bundle\FrameworkBundle\Controller\AbstractController
{

    public function liste(LieuRepository $lieuRepository, SerializerInterface $serializer){
        $lieux = $lieuRepository->findAll();

        $json=$serializer->serialize($lieux,'json', ['groups'=>'liste_produits']);

        return new JsonResponse($lieux);
    }


}