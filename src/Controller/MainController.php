<?php

namespace App\Controller;

use App\Entity\Participant;
use App\Repository\SortieRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    //test
    /**
     * @Route("/listSorties", name="main_home")
     */

    /*
    public function home()
    {
        return $this->render('main/home.html.twig');
    }
    */
    public function liste(SortieRepository $sortieRepository, EntityManagerInterface $entityManager): Response
    {
        //todo : changer le findAll par une rqt personnalisée


        $sorties = $sortieRepository->findSearch();
        //dd($sorties);
        return $this->render('main/home.html.twig', [
            "sorties"=>$sorties
        ]);
    }






}