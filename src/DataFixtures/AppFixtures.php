<?php

namespace App\DataFixtures;

use App\Entity\Campus;
use App\Entity\Participant;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{

    private UserPasswordEncoderInterface $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder){

        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $campus = new Campus();
        $campus->setNom('Nantes');


        $participant = new Participant();
        $participant->setNom('Salvatore');
        $participant->setPrenom('Sara');
        $participant->setTelephone('06 02 05 08 07');
        $participant->setEmail('sal.sara@testeni.fr');
        $plainPassword = 'testroot';
        $encoded = $this->encoder->encodePassword($participant, $plainPassword);
        $participant->setPassword($encoded);
        $participant->setAdministrateur(false);
        $participant->setActif(true);
        $participant->setCampus($campus);



        $manager->persist($participant);

        $manager->flush();
    }
}
